public interface Hotel {

    void isThereFreeRoom();
    int howMuchThereAreFreeRooms();
    int rentARoom(Custmer customer);
    int isThereAPossibilityToRenrRequestedNumbersOfRooms(int number);
    boolean isAPersonAlredyRentingARoom(Custmer customer);
    int[] checkHowManyRoomsIsAlredyRentedByACustomer(Custmer customer);
    void dropAllRoomsRentedByACustomer(Custmer customer);
}
