public class RoomNumber {

    int floor;
    int room;

    public RoomNumber(int floor, int room) {
        this.floor = floor;
        this.room = room;
    }

    public int getFloor() {
        return floor;
    }

    public int getRoom() {
        return room;
    }
}
