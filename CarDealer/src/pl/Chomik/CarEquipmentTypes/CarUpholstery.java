package pl.Chomik.CarEquipmentTypes;

import java.util.stream.Stream;

public enum CarUpholstery {

    PLASTIC(0),
    VELUR(1000),
    LEATHER(2500),
    ALCANTRA(6500),
    WOOD(10000);

    private int carUpholsteryProce;

    CarUpholstery (int carUpholsteryProce){
        this.carUpholsteryProce = carUpholsteryProce;
    }

    public static Stream<CarUpholstery> stream (){
        return Stream.of(CarUpholstery.values());
    }
    public int getCarUpholsteryProce(){
        return carUpholsteryProce;
    }


    }

