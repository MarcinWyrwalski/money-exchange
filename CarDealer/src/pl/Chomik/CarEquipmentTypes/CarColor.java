package pl.Chomik.CarEquipmentTypes;

import java.util.stream.Stream;

public enum CarColor {

    SNOWY_WHITE(0),
    SANDY_YELLOW(300),
    FOREST_GREEN(900),
    DEEP_BLUE(1800),
    CLASIC_SILVER(3900),
    NIGA_BLACK(6900),
    THE_FASTEST_RED(11500);

    public int carColourPrice;

    CarColor(int carColourPrice) {
        this.carColourPrice = carColourPrice;
    }

    public static Stream<CarColor> stream() {
        return Stream.of(CarColor.values());
    }

    public int getCarColourPrice() {
        return carColourPrice;
    }

}
