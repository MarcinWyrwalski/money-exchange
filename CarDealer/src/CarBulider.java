import pl.Chomik.CarEquipmentTypes.CarBodyType;
import pl.Chomik.CarEquipmentTypes.CarColor;
import pl.Chomik.CarEquipmentTypes.CarEngineType;
import pl.Chomik.CarEquipmentTypes.CarUpholstery;

public class CarBulider {
    private CarModels carModels;
    private CarColor carColor;
    private CarBodyType carBodyType;
    private CarUpholstery carUpholstery;
    private CarEngineType carEngineType;

    public static class Bulider {
        private CarModels carModels;
        private CarColor carColor;
        private CarBodyType carBodyType;
        private CarUpholstery carUpholstery;
        private CarEngineType carEngineType;

        public Bulider(CarModels carModels) {
            this.carModels = carModels;
        }

        public Bulider setupCarColor(CarColor carColor) {
            this.carColor = carColor;
            return this;
        }

        public Bulider setupCarBodyType(CarBodyType carBodyType) {
            this.carBodyType = carBodyType;
            return this;
        }

        public Bulider setupCarUpholseryType(CarUpholstery carUpholstery) {
            this.carUpholstery = carUpholstery;
            return this;
        }

        public Bulider setupCarEngineType(CarEngineType carEngineType) {
            this.carEngineType = carEngineType;
            return this;
        }

        CarModels getCarModels() {
            return carModels;
        }

        CarColor getCarColor() {
            return carColor;
        }

        CarBodyType getCarBodyType() {
            return carBodyType;
        }

        CarUpholstery getCarUpholstery() {
            return carUpholstery;
        }

        CarEngineType getCarEngineType() {
            return carEngineType;
        }

        @Override
        public String toString() {
            return "Bulider{" +
                    "carModels=" + carModels +
                    ", carColor=" + carColor +
                    ", carBodyType=" + carBodyType +
                    ", carUpholstery=" + carUpholstery +
                    ", carEngineType=" + carEngineType +
                    '}';
        }

        private CarEngineType getEngineType(CarBulider carBulider) {
            return carBulider.carEngineType;
        }

        private CarBulider bulid() {

            CarBulider carBulider = new CarBulider();
            carBulider.carModels = this.carModels;
            carBulider.carColor = this.carColor;
            carBulider.carBodyType = this.carBodyType;
            carBulider.carUpholstery = this.carUpholstery;
            carBulider.carEngineType = this.carEngineType;

            return carBulider;
        }

        void dispalyCurrentConfiguration(double amountOfmoney) {
            System.out.printf("%n Actual configuration: " +
                    "%n Model=" + carModels + " price: " + getCarModels().getCarPrice() +
                    "%n Color=" + carColor + " price: " + getCarColor().getCarColourPrice() +
                    "%n Body type=" + carBodyType + " price: " + getCarBodyType().getCarBodyTypePrice() +
                    "%n Upholstery=" + carUpholstery + " price: " + getCarUpholstery().getCarUpholsteryProce() +
                    "%n Engine=" + carEngineType + " price: " + getCarEngineType().getCarEngineTypePrice() +
                    "%n actual price: " + (+getCarModels().getCarPrice() + getCarColor().getCarColourPrice() + getCarBodyType().getCarBodyTypePrice() + getCarUpholstery().getCarUpholsteryProce() + getCarEngineType().getCarEngineTypePrice()) +

                    "%n amount of money remaining: " + amountOfmoney +
                    "%n");

            // above is terrible - need to rethink.
        }
    }
}
