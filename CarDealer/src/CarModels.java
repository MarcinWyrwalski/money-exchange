import java.util.stream.Stream;

public enum CarModels {

    AMG_C_63 (163612, "USD"),
    FABIA (65000, "PLN"),
    MICRA (85000,"51000"),
    GRAND_TURISMO (132975, "USD"),
    TOYOTA (85000,"PLN");

     int carPrice;
     String carCurrencySymbol;

      CarModels(int carPrice, String carCurrencyValue) {
        this.carPrice = carPrice;
        this.carCurrencySymbol = carCurrencySymbol;
    }
        public int getCarPrice() {
        return carPrice;
    }

    public String getCarCurrencySymbol() {
        return carCurrencySymbol;
    }

    public static Stream<CarModels> stream(){
        return Stream.of(CarModels.values());
    }
}
