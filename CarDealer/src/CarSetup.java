import java.util.stream.Stream;

public enum CarSetup {

    COLOR,
    BODY_STYLE,
    UPHOLSTERY,
    ENGINE_TYPE,
    DISPLAY,
    FINISH;

    CarSetup() {
    }

    public static Stream<CarSetup> stream(){
        return Stream.of(CarSetup.values());
    }


}
