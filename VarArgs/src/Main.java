public class Main{

    public int sumNumber(int x, int...xNumbers){
        for (   int i =0; i<xNumbers.length; i++){
            x += xNumbers[i];

        }
     return x;
    }

    public int sumNumberMultiple(int x,int y, int...xNumbers){
        x=x+y;
        for (   int i =0; i<xNumbers.length; i++){

            x += xNumbers[i];

        }
     return x;
    }


    public int sum (int number, int...argsNumber){
        int result = number;

        for (   int i =0; i<argsNumber.length; i++){
            result += argsNumber[i];


        }
            return result;
    }

    public static void main(String[] args) {

        Main main = new Main();
        System.out.println(main.sumNumber(2,2,2,2,2,2,2,2,2));

        System.out.println(main.sumNumberMultiple(3,3,1,2,3,4,5,6,9,8,7));
    }


}
