package pl.Chomik.Shapes;

import org.junit.Test;

import static org.junit.Assert.*;

public class RectangleTest {

    private Shapes rectangle = new Rectangle(3,4);

    @Test
    public void getArea() {

        double area = rectangle.getArea();

        assertEquals(area, 12, 0.0);

    }

    @Test
    public void getCircuit() {

        double circuit = rectangle.getCircuit();

        assertEquals(circuit, 14, 0.0);
    }
}