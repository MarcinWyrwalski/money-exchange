package pl.Chomik.Shapes;

import org.junit.Test;

import static org.junit.Assert.*;

public class RhombusTest {

    Shapes rhombus = new Rhombus(7,8);

    @Test
    public void getArea() {

        double area = rhombus.getArea();

        assertEquals(area, 28, 0.0);
    }

    @Test
    public void getCircuit() {

        double circuit = rhombus.getCircuit();

        assertEquals(circuit,56, 0.0);
    }
}