package pl.Chomik.Shapes;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class SquareTest {

    Shapes square = new Square(2);

    @Test
    public void getArea() {

        double area = square.getArea();

        assertEquals(area,4,0.0);
    }

    @Test
    public void getCircuit() {

        double circut = square.getCircuit();

        assertEquals(circut,8,0.0);
    }
}