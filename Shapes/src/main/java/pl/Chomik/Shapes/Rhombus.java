package pl.Chomik.Shapes;

public class Rhombus implements Shapes{
    private double width, height;

    Rhombus(double width, double height) {
        this.width = width;
        this.height = height;
    }

    @Override
    public double getArea() {
        return width*4;
    }

    @Override
    public double getCircuit() {
        return width*height;
    }
}
