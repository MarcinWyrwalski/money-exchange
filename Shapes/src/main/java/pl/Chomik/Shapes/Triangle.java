package pl.Chomik.Shapes;

public class Triangle implements Shapes {
     private double width,height,sideB, sideC;

    Triangle(double width, double height, double sideB, double sideC) {
        this.width = width;
        this.height = height;
        this.sideB = sideB;
        this.sideC = sideC;
    }

    @Override
    public double getArea() {
        return (0.5*width)*height;
    }

    @Override
    public double getCircuit() {
        return (width+sideB+sideC);
    }
}
