package pl.Chomik;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Driver;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Scanner;

public class ShopSQLConnectionManager {
     static final String DB_URL = "jdbc:mysql://localhost/hotel";


    void registerMySQLDriver() {
        try {
            Driver driver = new Driver();
            DriverManager.registerDriver(driver);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

     Connection connectToMySQLDB(String DB_URL, String USER, String PASS) {
        Connection connection = null;
        try {
            connection = (Connection) DriverManager.getConnection(DB_URL, USER, PASS);
        } catch (SQLException e) {
            e.printStackTrace();
        } return connection;
    }

      Connection start() {
      registerMySQLDriver();
     ;
          return userValidation();
      }

    Connection userValidation() {
        Scanner sc = new Scanner(System.in);
        System.out.println("User ID? ");
        String user_ID = sc.nextLine();
        System.out.println("User password?");
        String user_Pass = sc.nextLine();
        return connectToMySQLDB(DB_URL,user_ID,user_Pass);
    }
}
