package pl.Chomik;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.*;
import java.util.Scanner;

 class ShopSQLOperations {

    private String createQueryBulider(Connection connection) {

        // String bulider requied exact input  - if field is VARCHAR it must be inside ""
        // it should be possible to give an user help info into for loop using  modified / DBInformationRetrival
        // displaying name and type of field / field name (1?) field type (2?)

        Scanner sc = new Scanner(System.in);

        StringBuilder sb = new StringBuilder()
                .append("INSERT INTO ")
                .append("products ") // powinno być wybieralne lub dostarczane z systemu.
                .append("VALUES (");
        for (int i = 0; i < DBInformationRetrival(connection) - 1; i++) {

            System.out.println("Insert value nr: " + i);
            sb.append(sc.nextLine())
                    .append(",");
        }
        System.out.println("Insert last value: ");
        sb.append(sc.nextLine())
                .append(")");

        sc.close();

        return String.valueOf(sb);
    }

    void create(Connection connection) {
        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = connection.prepareStatement(createQueryBulider(connection));
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    int DBInformationRetrival(Connection connection) {

        PreparedStatement preparedStatement = null;
        int count = 0;

        try {
            preparedStatement = connection.prepareStatement("DESCRIBE products");
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                count++;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    void readAll(Connection connection) {
        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = connection.prepareStatement("SELECT * FROM products");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            ResultSet resultSet = preparedStatement.executeQuery();
            // how get rid of sout from here? return resultSet?
            while (resultSet.next()) {
                System.out.println(resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
