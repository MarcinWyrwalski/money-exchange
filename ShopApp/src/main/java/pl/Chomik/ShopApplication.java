package pl.Chomik;

import java.sql.Connection;

public class ShopApplication {

    public static void main(String[] args) {

        ShopSQLConnectionManager mg = new ShopSQLConnectionManager();
        Connection connection = mg.start();

        ShopSQLOperations sSQL = new ShopSQLOperations();
        sSQL.create(connection);
    }
}
