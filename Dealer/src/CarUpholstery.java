public enum CarUpholstery {

    VELOUR (0),
    LEATHER (1000),
    ALCANTRA (800),
    WOOD (1400),
    ;

    private int upholstery;

    CarUpholstery(int upholstery) {
        this.upholstery = upholstery;
    }

    public int getUpholstery() {
        return upholstery;
    }
}
