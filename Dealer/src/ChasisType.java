public enum  ChasisType {

    SEDAN (0),
    PICK_UP (2000),
    KOMBI (1000),
    HATCHBASK (1000);

    private  int chasisType;

    ChasisType(int chasisType) {
        this.chasisType = chasisType;
    }

    public int getChasisType() {
        return chasisType;
    }
}
