public enum FuelType {

    PETROL (0),
    DIESEL (10000),
    HYBRYD (15000);

    private int fuelType;

    FuelType(int fuelType) {
        this.fuelType = fuelType;
    }

    public int getFuelType() {
        return fuelType;
    }
}
