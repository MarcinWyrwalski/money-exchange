import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class streamExercise {

    private  static List<Apple> desiredAppleOutput(Apple... appleVarg){
        List<Apple> apples = Arrays.asList(appleVarg);

       return apples.stream()
                .filter(apple -> apple.colour.equals("red")  &&  apple.i >=3)
                .collect(Collectors.toList());

    }

    public static void main(String[] args) {


//        List<Apple> appleList = Arrays.asList(
//                new Apple("good", "red", 1),
//                new Apple("nice", "green", 5),
//                new Apple("good", "red", 3),
//                new Apple("not good", "red", 10),
//                new Apple("good", "green", 3),
//                new Apple("nice", "green", 5),
//                new Apple("good", "red", 3),
//                new Apple("not good", "red", 10),
//                new Apple("good", "green", 3),
//                new Apple("nice", "green", 5),
//                new Apple("good", "red", 3),
//                new Apple("not good", "red", 10),
//                new Apple("good", "green", 3),
//                new Apple("nice", "green", 5),
//                new Apple("good", "red", 3),
//                new Apple("not good", "red", 10),
//                new Apple("good", "green", 3),
//                new Apple("nice", "green", 5),
//                new Apple("good", "red", 3),
//                new Apple("not good", "red", 10),
//                new Apple("good", "green", 3),
//                new Apple("nice", "green", 5),
//                new Apple("good", "red", 3),
//                new Apple("not good", "red", 10),
//                new Apple("good", "green", 3),
//                new Apple("nice", "green", 5),
//                new Apple("good", "red", 3),
//                new Apple("not good", "red", 10),
//                new Apple("good", "green", 3),
//                new Apple("nice", "green", 5),
//                new Apple("good", "red", 3),
//                new Apple("not good", "red", 10),
//                new Apple("good", "green", 3),
//                new Apple("nice", "green", 5),
//                new Apple("good", "red", 3),
//                new Apple("not good", "red", 10),
//                new Apple("good", "green", 3),
//                new Apple("nice", "green", 5),
//                new Apple("good", "red", 3),
//                new Apple("not good", "red", 10),
//                new Apple("good", "green", 3),
//                new Apple("good", "red", 1),
//                new Apple("nice", "green", 5),
//                new Apple("good", "red", 3),
//                new Apple("not good", "red", 10),
//                new Apple("good", "green", 3),
//                new Apple("good", "red", 2));


//        appleList.stream()
////                .filter(apple -> apple.taste.equals("good"))
////                .filter(apple -> apple.colour.equals("red"))
////                .filter(apple -> apple.i > 1)
//                .filter(apple -> apple.i > DESIRED_WEIGHT_OF_AN_APPLES)
//                .filter(apple -> apple.colour != "red")
//                .findFirst();

//        Optional<Apple> notGreenApple = appleList.stream()
//                .filter(apple -> apple.i > DESIRED_WEIGHT_OF_AN_APPLES)
//                .filter(apple -> apple.colour.equals("red"))
//                .findFirst();

//        if (notGreenApple.get() != null){
//            throw new IllegalStateException("Znaleziono inny kolor");
//
//        }

        System.out.println(desiredAppleOutput(new Apple("good", "red", 1),
                new Apple("nice", "green", 5),
                new Apple("good", "red", 3),
                new Apple("not good", "red", 10),
                new Apple("good", "green", 3),
                new Apple("nice", "green", 5),
                new Apple("good", "red", 3),
                new Apple("not good", "red", 10),
                new Apple("good", "green", 3),
                new Apple("nice", "green", 5),
                new Apple("good", "red", 3),
                new Apple("not good", "red", 10),
                new Apple("good", "green", 3),
                new Apple("nice", "green", 5),
                new Apple("good", "red", 3),
                new Apple("not good", "red", 10),
                new Apple("good", "green", 3),
                new Apple("nice", "green", 5),
                new Apple("good", "red", 3),
                new Apple("not good", "red", 10),
                new Apple("good", "green", 3),
                new Apple("nice", "green", 5),
                new Apple("good", "red", 3),
                new Apple("not good", "red", 10),
                new Apple("good", "green", 3),
                new Apple("nice", "green", 5),
                new Apple("good", "red", 3),
                new Apple("not good", "red", 10),
                new Apple("good", "green", 3),
                new Apple("nice", "green", 5),
                new Apple("good", "red", 3),
                new Apple("not good", "red", 10),
                new Apple("good", "green", 3),
                new Apple("nice", "green", 5),
                new Apple("good", "red", 3),
                new Apple("not good", "red", 10),
                new Apple("good", "green", 3),
                new Apple("nice", "green", 5),
                new Apple("good", "red", 3),
                new Apple("not good", "red", 10),
                new Apple("good", "green", 3),
                new Apple("good", "red", 1),
                new Apple("nice", "green", 5),
                new Apple("good", "red", 3),
                new Apple("not good", "red", 10),
                new Apple("good", "green", 3),
                new Apple("good", "red", 2)));
    }
}
