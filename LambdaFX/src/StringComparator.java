public interface StringComparator {

    boolean compareString(String s1, String s2);
}
