import java.text.ParseException;
import java.util.Date;

public class Main {

    public static void main(String[] args) throws ParseException {



        String input = "29.02.2008";
        DateConverter dc = new DateConverter();
        FileNameFinder fnf = new FileNameFinder();
        System.out.println(dc.convertToDate(input));
        Date d = dc.convertToDate(input);
        System.out.println(dc.incrementByOneDay(d));
        System.out.println(dc.getYearFromDate(d));
        System.out.println(dc.parseDateToString(d));
        System.out.println(fnf.isFileExist("190103", "2019"));
//        System.out.println(fnf.regexBulider("190102"));
//        System.out.println(dc.parseToRequestedFormat(input));
    }
}
