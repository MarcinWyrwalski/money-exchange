import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;

public class FileNameFinder {

    public String regexBulider(String filename){
        StringBuilder result = new StringBuilder();
        result.append("c.*"+filename+".*");
        result.append("c.*190103.*");
        result.append(filename);
        result.append(".*");

        return result.toString();
    }

    public String isFileExist(String filename, String year) {
        BufferedReader bufferedReader = null;
        String name = null;
        String[] words = null;
        try {
            URL url = new URL("https://www.nbp.pl/kursy/xml/dir.txt");
            bufferedReader = new BufferedReader(new InputStreamReader(url.openStream()));

            String currentLine;

            while ((currentLine = bufferedReader.readLine()) != null) {
                if (currentLine.matches(regexBulider("190104"))) {
                    System.out.println(" znaleziono");
                    words = currentLine.split(" ");
                }
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Arrays.toString(words);
    }
}
